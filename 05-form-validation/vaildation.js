function validationForm() {
  let name = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let jberangkat = document.getElementById("jberangkat").value;
  let tkeberangkatan = document.getElementById("tkeberangkatan").value;
  let jtiket = document.getElementById("jtiket").value;

  console.log([name, email, jberangkat, tkeberangkatan, jtiket]);

  if (!name || name.length > 30) {
    document.getElementById("error-name").innerHTML = "ERROR!! nama wajib diisi maksimum 30 karakter.";
  } else if (!email || !email.match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
    document.getElementById("error-email").innerHTML = "ERROR!! email wajib diisi dan harus berbentuk alamat email.";
  } else if (!jberangkat) {
    document.getElementById("error-jberangkat").innerHTML = "ERROR!! jam keberangkatan wajib diisi.";
  } else if (!tkeberangkatan) {
    document.getElementById("error-tberangkat").innerHTML = "ERROR!! tiket keberangkatan wajib diisi.";
  } else if (!jtiket) {
    document.getElementById("error-jtiket").innerHTML = "ERROR!! jumlah tiket wajib diisi.";
  } else {
    let html = `
    <h2>Data Pemesanan</h2>
    <table>
      <tr>
        <td>Nama</td>
        <td>: <b>${name}</b></td>
      </tr>
      <tr>
        <td>Email</td>
        <td>: <b>${email}</b></td>
      </tr>
      <tr>
        <td>Jam Keberangkatan</td>
        <td>: <b>${jberangkat}</b></td>
      </tr>
      <tr>
        <td>Tujuan Keberangkatan</td>
        <td>: <b>${tkeberangkatan}</b></td>
      </tr>
      <tr>
        <td>Jumlah Tiket</td>
        <td>: <b>${jtiket}</b></td>
      </tr>
    </table>
    `
  
    document.getElementById("main").innerHTML = html;
  }

  
}