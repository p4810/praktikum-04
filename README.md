# tugas-javascript

Repository untuk pengumpulan tugas Pemrograman Web materi JavaScript Dasar STT Terpadu Nurul Fikri.

## Publikasi

Anda bisa membukanya pada site [https://kreasi.nurulfikri.ac.id/pisc21001ti/javascript-dasar/](https://kreasi.nurulfikri.ac.id/pisc21001ti/javascript-dasar)
